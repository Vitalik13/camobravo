//
//  Finance_AnalyticsApp.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 06.09.2023.
//

import SwiftUI

@main
struct Finance_AnalyticsApp: App {
    
    private let persistenceController = PersistenceManager()
    
    @StateObject private var userSettings = UserSettings()
    @StateObject private var notificationsManager = NotificationsManager()
    @StateObject private var sensitiveManager = SensitiveManager()
    @State private var isLocked = true

    var body: some Scene {
        WindowGroup {
            if isLocked {
                PincodeView(isLocked: $isLocked)
                    .environmentObject(userSettings)
                    .foregroundColor(.primary)
            } else {
                MainScreenView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
                    .environmentObject(userSettings)
                    .environmentObject(sensitiveManager)
                    .foregroundColor(.primary)
                    .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
                        isLocked = true
                    }.onAppear {
                        UIApplication.shared.applicationIconBadgeNumber = .zero
                        guard userSettings.isPushSet != true else { return }
                        notificationsManager.configure {
                            userSettings.isPushSet = true
                        }
                    }
            }
        }
    }
}
