//
//  Profile.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 17.09.2023.
//

import Foundation

struct Profile: Codable {
    var name: String?
    var image: Data?
}
