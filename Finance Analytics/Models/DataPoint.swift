//
//  DataPoint.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 30.09.2023.
//

import Foundation

struct DataPoint: Identifiable {
    
    let id = UUID().uuidString
    let value: Double
    let title: String
    
    init(value: Double, title: String) {
        self.value = value
        self.title = title
    }
}
