//
//  Budget.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 23.09.2023.
//

import Foundation

private extension Date {
    func isTheSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return Calendar.current.compare(self, to: date, toGranularity: component) == .orderedSame
    }
}

struct Budget: Codable, Identifiable {
    let date: Date
    let amount: Double
    var spent: Double
    
    var id: Date {
        date
    }
    
    var isExpired: Bool {
        !date.isTheSame(.weekOfYear, as: Date())
    }
}
