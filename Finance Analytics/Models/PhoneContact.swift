//
//  PhoneContact.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 14.10.2023.
//

import Foundation
import ContactsUI

extension String {
    var formattedPhone: String {
        if self == "+" {
          return self
        }
        
        var digitsString = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        let mask: String
        if digitsString.first == "8", self.first != "+" {
          digitsString.replaceSubrange((digitsString.startIndex ... digitsString.startIndex), with: "7")
        }
        if digitsString.first == "7" {
          mask = "+X XXX XXX XX XX"
        } else {
          mask = "+XXXXXXXXXXXXXXX"
        }
        
        var index = digitsString.startIndex
        return String(mask.compactMap({ m -> Character? in
          guard index < digitsString.endIndex else { return nil }
          if m == "X" {
            let c = digitsString[index]
            index = digitsString.index(after: index)
            return c
          } else {
            return m
          }
        }))
    }
}

struct PhoneContact: Identifiable {
    
    let id = UUID()
    let name: String
    var avatarImage: UIImage?
    var phoneNumber: String?
    
    init(contact: CNContact) {
        if contact.givenName.isEmpty && contact.familyName.isEmpty {
            name = "Unnamed"
        } else {
            name = contact.givenName + " " + contact.familyName
        }
        
        if let avatarData = contact.thumbnailImageData, let avatarImage = UIImage(data: avatarData) {
            self.avatarImage = avatarImage
        }
        
        phoneNumber = contact.phoneNumbers.first?.value.stringValue.formattedPhone
    }
}
