//
//  DBOperationModel.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 09.09.2023.
//
//

import Foundation
import CoreData

@objc(DBOperationModel)
public class DBOperationModel: NSManagedObject, Identifiable {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBOperationModel> {
        return NSFetchRequest<DBOperationModel>(entityName: "OperationModel")
    }

    @NSManaged public var id: String
    @NSManaged public var timestamp: Date
    @NSManaged public var title: String
    @NSManaged public var amount: Double
    @NSManaged public var imageData: Data?
    @NSManaged public var address: String?
    @NSManaged public var phone: String?
    @NSManaged public var audioData: Data?
}
