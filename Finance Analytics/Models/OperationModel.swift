//
//  OperationModel.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 09.09.2023.
//

import CoreData

private let operationDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    return formatter
}()

private let dayFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "d MMM"
    return formatter
}()

extension String {
    var operationDate: Date? {
        operationDateFormatter.date(from: self)
    }
    
    var dayFormat: Date? {
        dayFormatter.date(from: self)
    }
}

extension Date {
    var operationDateString: String {
        operationDateFormatter.string(from: self)
    }
    
    var dayFormat: String {
        dayFormatter.string(from: self)
    }
}

struct OperationModel: Identifiable {
  
    let id = UUID().uuidString
    let timestamp: Date
    let title: String
    let amount: Double
    let imageData: Data?
    let address: String?
    let phone: String?
    let audioData: Data?
    
    var amountString: String {
        String(amount)
    }
    
    static let stub = OperationModel(timestamp: Date(), title: "foo", amount: 100, imageData: nil, address: nil, phone: nil, audioData: nil)
}

extension OperationModel {
        
    static func from(dbModel: DBOperationModel) -> OperationModel {
        OperationModel(
            timestamp: dbModel.timestamp,
            title: dbModel.title,
            amount: dbModel.amount,
            imageData: dbModel.imageData,
            address: dbModel.address,
            phone: dbModel.phone,
            audioData: dbModel.audioData
        )
    }
    
    func createDB(viewContext: NSManagedObjectContext) {
        let dbOperation = DBOperationModel(context: viewContext)
        dbOperation.id = id
        dbOperation.timestamp = timestamp
        dbOperation.title = title
        dbOperation.amount = amount
        dbOperation.imageData = imageData
        dbOperation.address = address
        dbOperation.phone = phone
        dbOperation.audioData = audioData
    }
}
