//
//  CalendarEventsView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 19.10.2023.
//

import EventKitUI
import SwiftUI

struct CalendarEventsView: UIViewControllerRepresentable {
    
    let event: EKEvent
    let eventsStore: EKEventStore
    
    typealias UIViewControllerType = EKEventEditViewController
    
    func makeUIViewController(context: Context) -> EKEventEditViewController {
        let vc = EKEventEditViewController()
        vc.event = event
        vc.eventStore = eventsStore
        vc.editViewDelegate = context.coordinator
        return vc
    }
    
    func updateUIViewController(_ uiViewController: EKEventEditViewController, context: Context) {}
    
    func makeCoordinator() -> CalendarEventsView.Coordinator {
        Coordinator()
    }
    
    class Coordinator: NSObject, EKEventEditViewDelegate, UINavigationControllerDelegate {
        
        func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
            controller.dismiss(animated: true)
        }
    }
}
