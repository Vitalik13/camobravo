//
//  EventsManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 19.10.2023.
//

import EventKit
import SwiftUI

class EventsManager: ObservableObject {
    
    lazy var eventsStore = EKEventStore()
    
    func setup(completion: @escaping () -> Void) {
        if #available(iOS 17.0, *) {
            eventsStore.requestWriteOnlyAccessToEvents { _, _ in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else {
            eventsStore.requestAccess(to: .event) { _, _  in
                DispatchQueue.main.async {
                    completion()
                }
            }
        }
    }
    
    func createEvent(title: String, date: Date) -> EKEvent {
        let event = EKEvent(eventStore: eventsStore)
        event.title = title
        if let startDate = Calendar.current.date(byAdding: .weekOfYear, value: 1, to: date),
           let endDate = Calendar.current.date(byAdding: .hour, value: 1, to: startDate) {
            event.startDate = startDate
            event.endDate = endDate
        }
        return event
    }
}
