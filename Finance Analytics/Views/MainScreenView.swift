//
//  MainScreenView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 06.09.2023.
//

import SwiftUI

struct MainScreenView: View {
    
    var body: some View {
        NavigationView {
            EventsView()
            Text("")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenView()
    }
}
