//
//  ContactListCell.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 14.10.2023.
//

import SwiftUI

struct ContactListCell: View {
    
    let contact: PhoneContact
    
    var body: some View {
        HStack(spacing: 16) {
            avatarImage()
            VStack(alignment: .leading, spacing: 2) {
                Text(contact.name)
                    .font(.system(size: 17, weight: .semibold))
                Text(contact.phoneNumber ?? "")
                    .foregroundColor(.secondary)
                    .font(.system(size: 13))
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 8, leading: 16, bottom: 8, trailing: 16))
    }
    
    @ViewBuilder
    func avatarImage() -> some View {
        if let avatarImage = contact.avatarImage {
            Image(uiImage: avatarImage)
                .resizable()
                .frame(width: 40, height: 40)
                .aspectRatio(1, contentMode: .fit)
                .cornerRadius(20)
        } else {
            ZStack {
                defaultImage
                Text(contact.name.trimmingCharacters(in: .whitespaces).prefix(1))
                    .font(.system(size: 16))
                    .foregroundColor(.white)
            }
        }
    }
    
    var defaultImage: some View {
        Circle()
            .fill()
            .foregroundColor(.gray)
            .frame(width: 40, height: 40)
    }
}
