//
//  ContactsListView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 14.10.2023.
//

import SwiftUI

struct ContactsListView: View {
    
    @Binding var selectedPhone: String
    @State private var contacts = [String : [PhoneContact]]()
    @State private var isError = false
    @State private var isLoading = true
    
    @StateObject var phoneContactsService = PhoneContactsService()
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        NavigationView {
            Group {
                if isLoading {
                    HStack(spacing: 10) {
                        Text("Loading")
                            .font(.title2)
                        ProgressView()
                    }
                } else if isError || contacts.isEmpty {
                    Text(isError ? "Access to contacts is no allowed" : "No contacts")
                        .font(.title)
                } else {
                    ScrollView {
                        LazyVStack(alignment: .leading) {
                            ForEach(contacts.keys.sorted(), id: \.self) { key in
                                Section(header: Text(key)
                                    .font(.system(size: 15))
                                    .fontWeight(.semibold)
                                    .padding(EdgeInsets(top: 8, leading: 16, bottom: 8, trailing: 8))
                                ) {
                                    ForEach(contacts[key] ?? []) { contact in
                                        Button {
                                            selectedPhone = contact.phoneNumber ?? ""
                                            presentation.wrappedValue.dismiss()
                                        } label: {
                                            ContactListCell(contact: contact)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .navigationTitle("Contacts")
        }
        .onAppear {
            DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 0.1) {
                do {
                    let contacts = try phoneContactsService.loadContacts()
                    DispatchQueue.main.async {
                        self.contacts = contacts
                        isLoading = false
                    }
                } catch {
                    DispatchQueue.main.async {
                        isError = true
                        isLoading = false
                    }
                }
            }
        }
    }
}
