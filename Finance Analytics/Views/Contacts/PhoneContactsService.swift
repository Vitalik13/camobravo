//
//  PhoneContactsService.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 14.10.2023.
//

import Foundation
import ContactsUI

class PhoneContactsService: ObservableObject {
    
    private lazy var contactStore = CNContactStore()
    
    func loadContacts() throws -> [String: [PhoneContact]] {
        var allContacts = [String: [PhoneContact]]()
        do {
            let contacts = try getContacts()
            for contact in contacts {
                if !contact.phoneNumbers.isEmpty {
                    let contact = PhoneContact(contact: contact)
                    allContacts[String(contact.name.trimmingCharacters(in: .whitespaces).uppercased().prefix(1)), default: []].append(contact)
                }
            }
            return allContacts
        } catch {
            throw error
        }
    }
    
    private func getContacts() throws -> [CNContact] {
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactThumbnailImageDataKey] as? [CNKeyDescriptor] ?? []
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            throw FetchingContactThrows.accessError
        }
        
        var results: [CNContact] = []
        
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
                results.append(contentsOf: containerResults)
            } catch {
                throw FetchingContactThrows.conversationError
            }
        }
        return results
    }
}

enum FetchingContactThrows: String, Error {
    case accessError
    case conversationError
}
