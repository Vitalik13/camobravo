//
//  ProfileView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 17.09.2023.
//

import SwiftUI

struct ProfileView: View {
    
    @State private var name = ""
    @State private var image: UIImage?
    @State private var isImagePickerShown = false
    @State private var isCameraShown = false
    @State private var isAlertPresented = false
    @State private var isChangePinShown = false
    
    @EnvironmentObject var settings: UserSettings
    @EnvironmentObject var sensitiveManager: SensitiveManager
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 24) {
                    Button {
                        isAlertPresented = true
                    } label: {
                        imageView
                            .aspectRatio(1, contentMode: .fit)
                            .frame(width: 150)
                            .clipShape(Circle())
                    }
                    .overlay(
                        Button(action: {
                            self.image = nil
                        }, label: {
                            Image(systemName: image != nil ? "pencil.slash" : "")
                        }), alignment: .topTrailing
                    )
                    TextField("Name", text: $name)
                        .font(.title)
                    Button {
                        isChangePinShown = true
                    } label: {
                        HStack {
                            Text("Change pin")
                            Image(systemName: "lock.circle")
                        }.font(.title3)
                    }
                    .padding(.bottom)
                    ForEach(settings.budgetHistory) { item in
                        VStack {
                            HStack {
                                VStack(alignment: .leading) {
                                    HStack {
                                        Text("Amount: ")
                                        if sensitiveManager.isModeEnabled {
                                            Text(String(item.amount))
                                                .redacted(reason: .placeholder)
                                        } else {
                                            Text(String(item.amount))
                                        }
                                        Text("$")
                                    }
                                    HStack {
                                        Text("Spent: ")
                                        if sensitiveManager.isModeEnabled {
                                            Text(String(item.spent))
                                                .redacted(reason: .placeholder)
                                        } else {
                                            Text(String(item.spent))
                                        }
                                        Text("$")
                                    }
                                }
                                Spacer()
                                Text("\(item.date.operationDateString)")
                            }
                            Divider()
                        }
                    }
                }
                .padding()
                .padding(.top, 40)
            }
            .navigationTitle("Profile")
            .onAppear {
                name = settings.profile?.name ?? ""
                DispatchQueue.global(qos: .userInitiated).async {
                    if let imageData = settings.profile?.image, let image = UIImage(data: imageData) {
                        self.image = image
                    }
                }
            }
            .sheet(isPresented: $isImagePickerShown) {
                ImagePicker(sourceType: .photoLibrary) { image in
                    self.image = image
                }
            }
            .sheet(isPresented: $isCameraShown) {
                ImagePicker(sourceType: .camera) { image in
                    self.image = image
                }
            }
            .sheet(isPresented: $isChangePinShown) {
                PincodeView(isLocked: .constant(true), isChange: true)
            }
            .toolbar {
                ToolbarItem {
                    Button {
                        DispatchQueue.global(qos: .userInitiated).async {
                            var profile = settings.profile ?? Profile()
                            profile.name = name
                            profile.image = image?.pngData()
                            settings.profile = profile
                        }
                        presentation.wrappedValue.dismiss()
                    } label: {
                        Label("Save", systemImage: "square.and.arrow.down")
                            .opacity(isSaveDisabled ? 0.5 : 1)
                    }.disabled(isSaveDisabled)
                }
            }
            .actionSheet(isPresented: $isAlertPresented) {
                ActionSheet(
                    title: Text("Add photo"),
                    buttons: [
                        .default(Text("Take photo")) {
                            isCameraShown = true
                        },
                        .default(Text("Photo library")) {
                            isImagePickerShown = true
                        },
                        .cancel(Text("Cancel"))
                    ]
                )
            }
        }
    }
    
    @ViewBuilder
    private var imageView: some View {
        if let image {
            Image(uiImage: image)
                .resizable()
        } else {
            Image(systemName: "person.circle")
                .resizable()
        }
    }
    
    private var isSaveDisabled: Bool {
        image == nil && name.isEmpty
    }
}

