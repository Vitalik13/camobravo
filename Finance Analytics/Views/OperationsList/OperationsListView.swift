//
//  OperationsListView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 06.09.2023.
//

import SwiftUI

private extension FetchedResults<DBOperationModel> {
    var groupedByDay: [OperationHeaderView] {
        guard !self.isEmpty else { return [] }
        let grouped = Dictionary(grouping: self) { $0.timestamp.dayFormat }
        return grouped.map { day -> OperationHeaderView in
            OperationHeaderView(id: day.key, operations: day.value)
        }.sorted { $0.id > $1.id }
    }
}

private struct OperationHeaderView: Identifiable {
    let id: String
    var operations: [FetchedResults<DBOperationModel>.Element]
}

struct OperationsListView: View {
    
    let operationsService: OperationsService
    let items: FetchedResults<DBOperationModel>
    @Binding var budget: String
    @EnvironmentObject var settings: UserSettings
    var deleteAction: ((FetchedResults<DBOperationModel>.Element) -> Void)?
    
    private let dateFormatter = DateFormatter()
    private var groupedItems: [OperationHeaderView] {
        settings.budgetHistory.isEmpty ? [] : items.groupedByDay
    }
    private var dataPoints: [DataPoint] {
        dateFormatter.shortWeekdaySymbols.map { weekDay in DataPoint(value: getAmount(
            item: groupedItems.first(where: { item in
                if let date = item.id.dayFormat {
                    return dateFormatter.shortWeekdaySymbols[Calendar.current.component(.weekday, from: date) - 1] == weekDay
                } else {
                    return false
                }
            })
        ), title: weekDay) }
    }
    
    var body: some View {
        VStack {
            BarChart(dataPoints: dataPoints)
                .frame(height: 100)
                .padding(.horizontal)
            List {
                ForEach(groupedItems) { item in
                    Section(header: Text(item.id), footer: Text("Total: \(String(getAmount(item: item)))$")) {
                        ForEach(item.operations) { operation in
                            NavigationLink {
                                OperationDetailsView(dbOperation: operation, operationsService: operationsService)
                            } label: {
                                OperationCellView(operation: OperationModel.from(dbModel: operation))
                            }
                        }
                        .onDelete { offsets in
                            if let index = offsets.first {
                                let item = item.operations[index]
                                deleteAction?(item)
                            }
                        }
                    }
                }
            }
            .listStyle(.insetGrouped)
        }
        .overlay(Text(items.isEmpty ? "No operations yet" : "")
            .font(.title))
    }
    
    private func getAmount(item: OperationHeaderView?) -> Double {
        item?.operations.reduce(.zero, { $0 + $1.amount }) ?? .zero
    }
}
