//
//  OperationDetailsView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 09.09.2023.
//

import SwiftUI

struct OperationDetailsView: View {
    
    let dbOperation: DBOperationModel?
    let operationsService: OperationsService
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var title = ""
    @State private var amount = ""
    @State private var date = Date()
    @State private var image: UIImage?
    @State private var address = ""
    @State private var phone = ""
    @State private var isImagePickerShown = false
    @State private var isCameraShown = false
    @State private var isMapShown = false
    @State private var isContactsShown = false
    @State private var isPhotoAlertPresented = false
    @State private var isAudioRecording = false
    @State private var isAudioAvailable = false
    @State private var isEventShown = false
    
    @StateObject private var audioRecorderService = AudioRecorderService()
    @StateObject private var eventsManager = EventsManager()
    
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                Button {
                    isPhotoAlertPresented = true
                } label: {
                    imageView
                        .aspectRatio(1, contentMode: .fit)
                        .frame(width: 100)
                        .clipShape(Circle())
                }
                TextField("Title", text: $title)
                DatePicker("Date", selection: $date, in: ...Date(), displayedComponents: .date)
                HStack {
                    TextField("Amount", text: $amount)
                        .keyboardType(.decimalPad)
                    Text("$")
                }
                HStack {
                    TextField("Phone", text: $phone)
                        .keyboardType(.phonePad)
                    Button {
                        isContactsShown = true
                    } label: {
                        HStack {
                            Text("Show")
                            Image(systemName: "phone")
                        }
                    }
                }
                HStack {
                    TextField("Location", text: $address)
                    Button {
                        isMapShown = true
                    } label: {
                        HStack {
                            Text("Show")
                            Image(systemName: "mappin.circle")
                        }
                    }
                    .disabled(address.isEmpty)
                    .opacity(address.isEmpty ? 0.5 : 1)
                }
                HStack {
                    Button {
                        audioRecorderService.playOrPause()
                    } label: {
                        ZStack {
                            ZStack {
                                Circle()
                                    .fill(.red)
                                    .frame(width: 40, height: 40)
                                Image(systemName: audioRecorderService.isAudioPlaying ? "stop" : "play")
                                    .scaleEffect(1.2)
                            }
                        }
                    }
                    .disabled(isAudioButtonDisabled)
                    .opacity(isAudioButtonDisabled ? 0.5 : 1)
                    Spacer()
                    Text(audioStatus)
                    Spacer()
                    Button {
                        if isAudioRecording {
                            audioRecorderService.stopRecorder()
                            isAudioRecording = false
                        } else {
                            audioRecorderService.record()
                            isAudioRecording = true
                        }
                    } label: {
                        ZStack {
                            Circle()
                                .fill(.red)
                                .frame(width: 40, height: 40)
                            Image(systemName: "mic")
                                .scaleEffect(1.2)
                        }
                    }
                    .disabled(!isAudioAvailable)
                    .opacity(isAudioAvailable ? 1 : 0.5)
                    .scaleEffect(isAudioRecording ? 1.2 : 1)
                    .animation(isAudioRecording ? Animation.easeInOut(duration: 1.5).repeatForever(autoreverses: true) : .default, value: isAudioRecording)
                }
                Button {
                    eventsManager.setup {
                        isEventShown = true
                    }
                } label: {
                    HStack {
                        Text("Repeat next week")
                        Image(systemName: "calendar.badge.plus")
                    }
                    .font(.title2)
                }
            }.padding()
        }
        .navigationTitle("\(dbOperation != nil ? "Edit" : "New") operation")
        .toolbar {
            ToolbarItem {
                Button {
                    DispatchQueue.global(qos: .userInitiated).async {
                        let newOperation = OperationModel(
                            timestamp: date,
                            title: title,
                            amount: Double(amount) ?? 0,
                            imageData: image?.pngData(),
                            address: address.isEmpty ? nil : address,
                            phone: phone,
                            audioData: audioRecorderService.audio
                        )
                        operationsService.addOrUpdate(dbOperation: dbOperation, newOperation: newOperation, viewContext: viewContext)
                    }
                    presentation.wrappedValue.dismiss()
                } label: {
                    Label("Save", systemImage: "square.and.arrow.down")
                        .opacity(isSaveButtonDisabled ? 0.5 : 1)
                }
                .disabled(isSaveButtonDisabled)
            }
        }
        .onAppear {
            audioRecorderService.setup { isGranted in
                isAudioAvailable = isGranted
            }
            guard let dbOperation = dbOperation else { return}
            DispatchQueue.global(qos: .userInitiated).async {
                let operation = OperationModel.from(dbModel: dbOperation)
                title = operation.title
                date = operation.timestamp
                amount = operation.amountString
                address = operation.address ?? ""
                phone = operation.phone ?? ""
                if let imageData = operation.imageData, let image = UIImage(data: imageData) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.image = image
                    }
                }
                if let audioData = operation.audioData {
                    DispatchQueue.main.async {
                        audioRecorderService.audio = audioData
                    }
                }
            }
        }
        .onChange(of: phone) { newValue in
            phone = newValue.formattedPhone
        }
        .actionSheet(isPresented: $isPhotoAlertPresented) {
            ActionSheet(
                title: Text("Add photo"),
                buttons: [
                    .default(Text("Take photo")) {
                        isCameraShown = true
                    },
                    .default(Text("Photo library")) {
                        isImagePickerShown = true
                    },
                    .cancel(Text("Cancel"))
                ]
            )
        }
        .sheet(isPresented: $isImagePickerShown) {
            ImagePicker(sourceType: .photoLibrary) { image in
                self.image = image
            }
        }
        .sheet(isPresented: $isCameraShown) {
            ImagePicker(sourceType: .camera) { image in
                self.image = image
            }
        }
        .sheet(isPresented: $isMapShown) {
            MapScreenView(address: address)
        }
        .sheet(isPresented: $isContactsShown) {
            ContactsListView(selectedPhone: $phone)
        }
        .sheet(isPresented: $isEventShown) {
            CalendarEventsView(
                event: eventsManager.createEvent(title: title, date: date),
                eventsStore: eventsManager.eventsStore
            )
        }
    }
    
    @ViewBuilder
    private var imageView: some View {
        if let image {
            Image(uiImage: image)
                .resizable()
        } else {
            Image(systemName: "photo.on.rectangle.angled")
                .resizable()
                .padding()
        }
    }
    
    private var isSaveButtonDisabled: Bool {
        title.isEmpty || amount.isEmpty
    }
    
    private var isAudioButtonDisabled: Bool {
        isAudioRecording || audioRecorderService.audio == nil
    }
    
    private var audioStatus: String {
        if !isAudioAvailable {
            return "Access to mic is no allowed"
        } else if audioRecorderService.audio == nil {
            return "No audio"
        }
        return "Ready to play"
    }
}

struct OperationDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        OperationDetailsView(dbOperation: nil, operationsService: OperationsService())
    }
}
