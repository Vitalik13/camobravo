//
//  OperationCellView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 09.09.2023.
//

import SwiftUI

struct OperationCellView: View {
    
    let operation: OperationModel
    
    @EnvironmentObject var sensitiveManager: SensitiveManager
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(operation.title)
                Text(operation.timestamp.operationDateString)
            }
            Spacer()
            if sensitiveManager.isModeEnabled {
                Text(String(operation.amount))
                    .redacted(reason: .placeholder)
            } else {
                Text(String(operation.amount))
            }
            Text("$")
        }
    }
}

struct OperationCellView_Previews: PreviewProvider {
    static var previews: some View {
        OperationCellView(operation: .stub)
    }
}
