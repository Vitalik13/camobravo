//
//  MapScreenView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 25.09.2023.
//

import SwiftUI
import MapKit

struct AnnotationView: View {
            
    var body: some View {
        Circle()
            .foregroundColor(.white)
            .frame(width: 30, height: 30)
            .overlay(
                Circle().strokeBorder(.blue, lineWidth: 4)
                , alignment: .center)
    }
}

struct MapScreenView: View {
    
    private struct Point: Codable, Identifiable {
        let lat: Double
        let lon: Double
        
        var id: String {
            String((lat + lon))
        }
        var coordinates: CLLocationCoordinate2D {
            CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
    }
    
    @StateObject private var locationManager = LocationManager()
    
    @State private var region = MKCoordinateRegion(center: .init(), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
    @State private var isError = false
    @State private var annotations = [Point]()
    
    @Environment(\.presentationMode) var presentation
    
    let address: String
    
    var body: some View {
        Map(coordinateRegion: $region,
            interactionModes: .all,
            showsUserLocation: true,
            annotationItems: annotations) { annotation in
            MapAnnotation(coordinate: annotation.coordinates) {
                AnnotationView()
            }
        }.overlay(RoundedRectangle(cornerRadius: 1)
            .frame(width: 40, height: 2)
            .foregroundColor(.gray)
            .padding(.top)
                  , alignment: .top)
        .overlay(Rectangle().fill(
            LinearGradient(gradient: Gradient(colors: [.white, .black]),
                           startPoint: .bottom, endPoint: .top
                          )).frame(height: 44).opacity(0.1)
                 , alignment: .top)
        .alert(isPresented: $isError, content: {
            Alert(title: Text("Please try again"),
                  dismissButton: .default(Text("OK"), action: {
                presentation.wrappedValue.dismiss() }))
        })
        .onAppear {
            locationManager.getCoordinates(for: address)
        }
        .onReceive(locationManager.$pointLocation, perform: { location in
            guard let location else { return }
            let point = Point(lat: location.latitude, lon: location.longitude)
            annotations.insert(point, at: .zero)
            withAnimation(.easeInOut) {
                region.center = location
            }
        })
        .onReceive(locationManager.$isError, perform: { isError in
            self.isError = isError
        })
        .overlay(
            locationButton
                .padding(.trailing, 16)
                .offset(y: -70)
            , alignment: .bottomTrailing
        )
        .ignoresSafeArea()
    }
    
    @ViewBuilder
    private var locationButton: some View {
        if let userLocation = locationManager.userLocation {
            Button {
                withAnimation(.easeInOut) {
                    region.center = userLocation
                }
            } label: {
                Image(systemName: "location.fill")
                    .padding()
                    .background(Color.gray)
                    .foregroundColor(.white)
                    .clipShape(Circle())
                    .padding()
            }
        } else {
            EmptyView()
        }
    }
}
