//
//  EventsView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 17.09.2023.
//

import SwiftUI

struct EventsView: View {
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \DBOperationModel.timestamp, ascending: true)],
        animation: .default)
    private var items: FetchedResults<DBOperationModel>
    @Environment(\.managedObjectContext) private var viewContext
    
    @EnvironmentObject var settings: UserSettings
    @EnvironmentObject var sensitiveManager: SensitiveManager
    
    @StateObject private var operationsService = OperationsService()
    
    @State private var isClearAllAlertShown = false
    @State private var isDetailsPresented = false
    @State private var isProfilePresented = false
    @State private var budget = ""
    @State private var isSensitiveModeEnabled = false
    
    var body: some View {
        NavigationLink(
            destination: OperationDetailsView(
                dbOperation: nil,
                operationsService: operationsService
            ),
            isActive: $isDetailsPresented, label: {}
        )
        VStack {
            if settings.budgetHistory.isEmpty || settings.budgetHistory.last?.isExpired == true {
                VStack(spacing: 30) {
                    Text("Enter budget")
                        .font(.title)
                    HStack {
                        TextField("0", text: $budget)
                            .keyboardType(.decimalPad)
                        Text("$")
                    }.padding(.horizontal, 100)
                    Button {
                        withAnimation {
                            settings.budgetHistory.append(Budget(date: Date(), amount: Double(budget) ?? 0, spent: 9))
                        }
                    } label: {
                        Text("Confirm")
                            .font(.title)
                    }
                    .opacity(budget.isEmpty ? 0.5 : 1)
                    .disabled(budget.isEmpty)
                }
            } else {
                HStack {
                    Text("Budget ")
                    if sensitiveManager.isModeEnabled {
                        budgetText
                            .redacted(reason: .placeholder)
                    } else {
                        budgetText
                    }
                    Text("$")
                }
                .font(.title2)
                .padding()
                OperationsListView(
                    operationsService: operationsService,
                    items: items,
                    budget: $budget
                ) { item in
                    deleteItems(item: item)
                }
                .overlay(Button(action: {
                    isDetailsPresented = true
                }, label: {
                    Label("Add", systemImage: "plus")
                        .foregroundColor(.white)
                        .font(.title3)
                        .padding()
                        .background(Color.blue)
                        .clipShape(Capsule())
                }).padding(.bottom), alignment: .bottom)
            }
        }
        .navigationTitle("Finance Analytics")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                Button {
                    isClearAllAlertShown = true
                } label: {
                    Label("Clear all", systemImage: "trash")
                        .foregroundColor(.red)
                }
                .opacity(settings.budgetHistory.isEmpty ? 0.5 : 1)
                .disabled(settings.budgetHistory.isEmpty)
            }
            ToolbarItem {
                Button {
                    isProfilePresented = true
                } label: {
                    Image(systemName: "person.circle")
                        .foregroundColor(.blue)
                }
            }
        }
        .alert(isPresented: $isClearAllAlertShown) {
            Alert(
                title: Text("Are you sure?"),
                primaryButton: .destructive(Text("Remove"), action: {
                    withAnimation {
                        settings.pin = nil
                        settings.budgetHistory = []
                        budget = ""
                        operationsService.deleteItems(viewContext: viewContext, items: items)
                    }
                }),
                secondaryButton: .cancel()
            )
        }
        .sheet(isPresented: $isProfilePresented) {
            ProfileView()
        }
        .onAppear {
            calculateAndUpdateBudget()
            sensitiveManager.setup()
        }
    }
    
    private var budgetText: some View {
        Text(String(budget))
            .foregroundColor(Double(budget) ?? 0 < 0 ? .red : .primary)
    }
    
    private func calculateAndUpdateBudget() {
        if let spent = settings.calculateAndUpdateBudget(items: items) {
            budget = String(spent)
        }
    }
    
    private func deleteItems(item: FetchedResults<DBOperationModel>.Element) {
        withAnimation {
            operationsService.deleteItems(viewContext: viewContext, items: items, item: item)
        }
        calculateAndUpdateBudget()
    }
}
