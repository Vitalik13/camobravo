//
//  PincodeView.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 05.10.2023.
//

import SwiftUI
import LocalAuthentication

struct PincodeView: View {
    
    @State private var typedPass = ""
    @State private var repeatValue = ""
    @State private var isRepeat = false
    @State private var isError = false
    @State private var isChanging = false
    @State private var isFaceId = true
    @State private var isBiometryReady = false
    
    @Binding var isLocked: Bool
    let isChange: Bool
    
    private let context = LAContext()
    
    init(isLocked: Binding<Bool>, isChange: Bool = false) {
        self._isLocked = isLocked
        self.isChange = isChange
    }
    
    @EnvironmentObject var settings: UserSettings
    @Environment(\.presentationMode) var presentation
    
    private var columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        VStack(spacing: 16) {
            Text(title)
                .font(.title)
                .fontWeight(.semibold)
            HStack(spacing: 20) {
                ForEach((1...4), id: \.self) { point in
                    Circle()
                        .frame(width: 25)
                        .foregroundColor(typedPass.count >= point ? (isError ? .red : .blue) : .gray.opacity(0.5))
                        .scaleEffect(typedPass.count == 4 ? 1.5 : 1)
                }
            }
            Spacer()
            LazyVGrid(
                columns: columns,
                spacing: 16
            ) {
                ForEach(1...9, id: \.self) { num in
                    digitView(num: String(num))
                }
            }
            HStack {
                Button {
                    useBiometrics()
                } label: {
                    Image(systemName: isFaceId ? "faceid" : "touchid")
                        .font(.title)
                        .padding(24)
                }
                .disabled(!isBiometryReady)
                .opacity(!isBiometryReady ? 0 : 1)
                Spacer()
                digitView(num: "0")
                Spacer()
                Button {
                    typedPass.removeLast()
                } label: {
                    Image(systemName: "delete.left")
                        .font(.title)
                        .padding(24)
                }
                .disabled(typedPass.isEmpty)
                .opacity(typedPass.isEmpty ? 0 : 1)
            }
        }
        .padding(.vertical)
        .padding(.horizontal, 36)
        .padding(.top)
        .onAppear {
            guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) else { return }
            if context.biometryType == .touchID {
                isFaceId = false
            }
            if !isChange, settings.pin != nil {
                isBiometryReady = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    useBiometrics()
                }
            }
        }
    }
    
    private func digitView(num: String) -> some View {
        Button {
            isError = false
            typedPass.append(num)
            if typedPass.count == 4 {
                if let pin = settings.pin, !isChanging {
                    if typedPass == pin {
                        if isChanging {
                            isChanging = false
                            handleRepeat()
                        } else if isChange, !isRepeat {
                            isChanging = true
                            withAnimation {
                                typedPass = ""
                            }
                        } else {
                            withAnimation {
                                isLocked = false
                            }
                        }
                    } else {
                        isError = true
                        withAnimation {
                            typedPass = ""
                        }
                    }
                } else if isRepeat {
                    if repeatValue == typedPass {
                        settings.pin = typedPass
                        if isChange {
                            presentation.wrappedValue.dismiss()
                        }
                        isLocked = false
                    } else {
                        isError = true
                        withAnimation {
                            isRepeat = false
                        }
                    }
                    typedPass = ""
                } else {
                    handleRepeat()
                }
            }
        } label: {
            Text(num)
                .font(.title)
                .fontWeight(.semibold)
                .padding(24)
                .background(Color.gray.opacity(0.5))
                .clipShape(Circle())
        }
    }
    
    private var title: String {
        if isRepeat {
            return "Repeat"
        } else if isChange {
            if !isChanging {
                return "Enter previous pin"
            }
        }
        return "Enter pin"
    }
    
    private func handleRepeat() {
        isRepeat = true
        repeatValue = typedPass
        withAnimation {
            typedPass = ""
        }
    }
    
    private func useBiometrics() {
        let reason = "You can use biometrics to unlock your data."
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, _ in
            if success {
                DispatchQueue.main.async {
                    isLocked = false
                }
            }
        }
    }
}

#Preview {
    PincodeView(isLocked: .constant(true))
}
