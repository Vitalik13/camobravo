//
//  BarChart.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 30.09.2023.
//

import SwiftUI

struct BarChart: View {
    
    let dataPoints: [DataPoint]
    let maxValue: Double
    
    init(dataPoints: [DataPoint]) {
        self.dataPoints = dataPoints
        let highestPoint = dataPoints.max { $0.value < $1.value }
        maxValue = highestPoint?.value ?? 1
    }
    
    var body: some View {
        HStack {
            ForEach(dataPoints) { data in
                VStack {
                    Rectangle()
                        .fill(Color.accentColor)
                        .cornerRadius(2.5)
                        .scaleEffect(y: maxValue == 0 ? 0 : CGFloat(data.value / maxValue), anchor: .bottom)
                    Text(data.title)
                }
            }
        }
    }
}
