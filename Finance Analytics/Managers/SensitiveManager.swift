//
//  SensitiveManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 21.10.2023.
//

import UIKit
import CoreMotion

final class SensitiveManager: UIResponder, ObservableObject {
        
    @Published var isModeEnabled = false
    
    private let motionQueue = OperationQueue()
    private let feedbackGenerator = UINotificationFeedbackGenerator()
    private var isModeChanged = false
    private let motionManager: CMMotionManager = {
        let manager = CMMotionManager()
        manager.deviceMotionUpdateInterval = 1 / 60
        return manager
    }()
    
    func setup() {
        guard motionManager.isDeviceMotionAvailable, !motionManager.isDeviceMotionActive else { return }
        motionManager.startDeviceMotionUpdates(to: motionQueue) { (data, _) in
            guard let data else { return }
            var pitch = data.attitude.pitch
            if pitch > .zero {
                pitch = .pi - pitch
            } else {
                pitch = -(.pi + pitch)
            }
            if data.gravity.z > .zero, data.attitude.roll > 1.5 || pitch  > 1.5 {
                if !self.isModeChanged {
                    self.isModeChanged = true
                    self.feedbackGenerator.notificationOccurred(.success)
                    DispatchQueue.main.async {
                        self.isModeEnabled.toggle()
                    }
                }
            } else if self.isModeChanged {
                self.isModeChanged = false
            }
        }
    }
}
