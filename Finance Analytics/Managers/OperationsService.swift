//
//  OperationsService.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 09.09.2023.
//

import CoreData
import SwiftUI

final class OperationsService: ObservableObject {
            
    func addOrUpdate(dbOperation: DBOperationModel?, newOperation: OperationModel, viewContext: NSManagedObjectContext) {
        if let dbOperation = dbOperation {
            dbOperation.amount = newOperation.amount
            dbOperation.timestamp = newOperation.timestamp
            dbOperation.title = newOperation.title
            dbOperation.imageData = newOperation.imageData
            dbOperation.address = newOperation.address
            dbOperation.phone = newOperation.phone
            dbOperation.audioData = newOperation.audioData
        } else {
            newOperation.createDB(viewContext: viewContext)
        }
        saveContext(viewContext: viewContext)
    }
    
    func deleteItems(
        viewContext: NSManagedObjectContext,
        items: FetchedResults<DBOperationModel>,
        item: FetchedResults<DBOperationModel>.Element? = nil
    ) {
        if let item {
            viewContext.delete(item)
        } else {
            items.forEach(viewContext.delete)
        }
        saveContext(viewContext: viewContext)
    }
    
    private func saveContext(viewContext: NSManagedObjectContext) {
        do {
            try viewContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}
