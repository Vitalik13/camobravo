//
//  PersistenceManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 06.09.2023.
//

import CoreData

class PersistenceManager {
    
    let container: NSPersistentContainer

    init() {
        container = NSPersistentContainer(name: "Finance_Analytics")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        container.viewContext.automaticallyMergesChangesFromParent = true
    }
}
