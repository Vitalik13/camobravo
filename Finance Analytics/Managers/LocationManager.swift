//
//  LocationManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 25.09.2023.
//

import Foundation
import CoreLocation
import Combine

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    private let locationManager = CLLocationManager()
    private let geoCoder = CLGeocoder()
    @Published var userLocation: CLLocationCoordinate2D?
    @Published var pointLocation: CLLocationCoordinate2D?
    @Published var isError = false
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last, userLocation == nil else { return }
        userLocation = location.coordinate
    }
    
    func getCoordinates(for address: String) {
        geoCoder.geocodeAddressString(address) { [weak self] (placemarks, _) in
            guard let placemarks = placemarks, let coordinate = placemarks.first?.location?.coordinate else {
                self?.isError = true
                return
            }
            self?.pointLocation = coordinate
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}
