//
//  AudioRecorderService.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 15.10.2023.
//

import AVKit

final class AudioRecorderService: NSObject, ObservableObject, AVAudioPlayerDelegate {
    
    private var session = AVAudioSession.sharedInstance()
    private var recorder: AVAudioRecorder?
    private var player: AVAudioPlayer?
    private let fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("data.m4a")
    
    @Published var audio: Data?
    @Published var isAudioPlaying = false
    
    func setup(completion: @escaping (Bool) -> Void) {
        try? session.setCategory(.playAndRecord)
        session.requestRecordPermission { isGranted in
            DispatchQueue.main.async {
                completion(isGranted)
            }
        }
    }
    
    func record() {
        stopPlayer()
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        do {
            recorder = try AVAudioRecorder(url: fileURL, settings: settings)
            recorder?.record()
        } catch {
            print("Recorder", error.localizedDescription)
        }
    }
    
    func stopRecorder() {
        recorder?.stop()
        do {
            audio = try Data(contentsOf: fileURL)
        } catch {
            print("Failed to save audio", error.localizedDescription)
        }
    }
    
    func playOrPause() {
        if isAudioPlaying {
            stopPlayer()
        } else {
            guard let audio = audio else { return }
            player = try? AVAudioPlayer(data: audio)
            player?.prepareToPlay()
            player?.play()
            player?.delegate = self
            isAudioPlaying = true
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        isAudioPlaying = false
    }
    
    private func stopPlayer() {
        player?.stop()
        isAudioPlaying = false
    }
}
