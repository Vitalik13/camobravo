//
//  NotificationsManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 07.10.2023.
//

import UIKit
import UserNotifications

class NotificationsManager: NSObject, ObservableObject, UNUserNotificationCenterDelegate {
    
    private let notificationCenter = UNUserNotificationCenter.current()
    
    func configure(completion: @escaping () -> Void) {
        notificationCenter.delegate = self
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] (granted, error) in
            guard granted else { return }
            self?.notificationCenter.getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                self?.scheduleNotification(title: "Time to set your budget", completion: completion)
            }
        }
    }
    
    private func scheduleNotification(title: String, completion: @escaping () -> Void) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.categoryIdentifier = "alarm"
        content.badge = 1
        content.sound = .default
        
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.weekday = 1
        dateComponents.hour = 10
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error\(error.localizedDescription)")
            } else {
                completion()
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.sound])
    }
}
