//
//  AppSettingsManager.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 10.09.2023.
//

import SwiftUI
import Combine

@propertyWrapper
struct AppSettingCollection<T: Codable> {
    
    let key: String
    
    init(_ key: String) {
        self.key = key
    }
    
    var wrappedValue: [T] {
        get {
            if let data = UserDefaults.standard.value(forKey: key) as? Data {
                return (try? JSONDecoder().decode([T].self, from: data)) ?? []
            }
            return []
        }
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            UserDefaults.standard.setValue(data, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
}

@propertyWrapper
struct AppStorageData<T: Codable> {
    
    let key: String
    
    init(_ key: String) {
        self.key = key
    }
    
    var wrappedValue: T? {
        get {
            if let data = UserDefaults.standard.value(forKey: key) as? Data {
                return (try? JSONDecoder().decode(T.self, from: data))
            }
            return nil
        }
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            UserDefaults.standard.setValue(data, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
}

final class UserSettings: ObservableObject {
        
    private enum Keys: String {
        case budget, profile, pin, isPushSet
    }
    
    @AppStorage(Keys.isPushSet.rawValue)
    var isPushSet: Bool?
    
    @KeychainValue<String>(key: Keys.pin.rawValue) var pin: String?
    
    @AppStorageData(Keys.profile.rawValue)
    var profile: Profile?
    
    @AppSettingCollection(Keys.budget.rawValue)
    var budgetHistory: [Budget] {
        willSet {
            objectWillChange.send()
        }
    }
    
    func calculateAndUpdateBudget(items: FetchedResults<DBOperationModel>) -> Double? {
        guard var lastBudget = budgetHistory.last else { return nil }
        let spent = lastBudget.amount - items.reduce(.zero, { $0 + $1.amount })
        lastBudget.spent = spent
        budgetHistory[budgetHistory.count - 1] = lastBudget
        return spent
    }
}
