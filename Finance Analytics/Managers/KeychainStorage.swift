//
//  KeychainStorage.swift
//  Finance Analytics
//
//  Created by Alex Yatsenko on 07.10.2023.
//

import Foundation

class KeychainStorage {
    
    private init() {}
    public static let keychain = KeychainStorage()
    private let bundle = Bundle.main.bundleIdentifier ?? ""
    
    private func _get(key: String) -> Data? {
        let keychainItem = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: bundle,
            kSecAttrAccount: key,
            kSecReturnData: true,
        ] as CFDictionary
        var ref: AnyObject?
        let status = SecItemCopyMatching(keychainItem, &ref)
        if status == errSecItemNotFound { return nil }
        if status == errSecSuccess {
            return ref as? Data
        }
        print("Operation finished with status: \(status)")
        return nil
    }
    private func _del(key: String) {
        let keychainItem = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: bundle,
            kSecAttrAccount: key,
        ] as CFDictionary
        let status = SecItemDelete(keychainItem)
        if status == errSecItemNotFound { return }
        if status != errSecSuccess {
            print("Operation finished with status: \(status)")
        }
    }
    private func _set(key: String, value: Data) {
        let keychainItem = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: bundle,
            kSecAttrAccount: key,
            kSecAttrSynchronizable: false,
            kSecValueData: value,
        ] as CFDictionary
        let status = SecItemAdd(keychainItem, nil)
        if status == errSecDuplicateItem {
            _del(key: key)
            _set(key: key, value: value)
            return
        }
        if status != errSecSuccess {
            print("Operation finished with status: \(status)")
        }
    }
    
    subscript (_ key: String) -> Data? {
        get { _get(key: key) }
        set (value) {
            if let value = value {
                _set(key: key, value: value)
            } else {
                _del(key: key)
            }
        }
    }
}

@propertyWrapper struct KeychainValue<Value> {
    private let storage = KeychainStorage.keychain
    let key: String
    
    var wrappedValue: String? {
        get {
            guard let data = storage[key] else { return nil }
            return String(data: data, encoding: .utf8)
        }
        set { storage[key] = newValue?.data(using: .utf8) }
    }
}
